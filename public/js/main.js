

var requestForm = $("#request_form");
var requestFormBtn = $("#request_form_btn");
var requestProductForm = $("#request_product_form");
var requestProductFormBtn = $("#request_product_form_btn");
var requestFormSecond = $("#request_form1");
var requestFormBtnSecond = $("#request_form1_btn");
var requestThirdForm = $("#request_form2");
var requestThirdFormBtn = $("#request_form2_btn");
var requestContactForm = $("#request-contact-form");
var requestContactBtn = $("#request-contact-form-btn");

var map;

// DG.then(function () {
//     map = DG.map('map', {
//         center: [43.29172198143585,76.98404403367938],
//         zoom: 17
//     });
//      DG.marker([43.29172198143585,76.98404403367938]).addTo(map);
// });

$('.phone').mask('+7(999)-999-99-99');




$('.popap-drawing').magnificPopup({
    type: 'image'
});

$('.certificate-slider').owlCarousel({
    center: true,
    items:3,
    loop:true,
    smartSpeed: 700,
    autoplay: true,
    autoplayTimeout: 4000,
    responsive:{
        1024:{
            items:5
        },
        600:{
            items:1
        }
    }
});

$('.image-popup-zoom').magnificPopup({
    type: 'image',
    zoom: {
        enabled: true,
        duration: 300,
    }
});


   // catalog image tab
   $('.item-galery img').click(function(){
    let attr = $(this).attr('src');
    $('.item-galery').removeClass('activ-slide');
    $(this).parent('.item-galery').toggleClass('activ-slide');

    $('.galery-main img').attr('src', attr);
});

window.onload = function () {
    var colorImages = document.getElementsByClassName('color');

    for (var j = 0; j < colorImages[0].childElementCount; j++) {
        colorImages[0].children[j].addEventListener('click', function() {
            this.classList.add("activ-slide-color");
        });
    }
}


// $('.burger-menu-btn').click(function(e){
//     e.preventDefault();
//     $(this).toggleClass('burger-menu-lines-active');
//     $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
//     $('body').toggleClass('body-overflow');
// });


   var itemGalery = document.querySelectorAll('.item-galery img');
   var colorGalery = document.querySelectorAll('.color img');
   for(const i of colorGalery){
    i.addEventListener('click', function(){
        var color = i.getAttribute('data-color');
        $(".item-galery").removeClass('activ-slide')
        document.querySelector('.item-galery img[data-color="'+color+'"]').parentNode.classList += ' activ-slide';
        document.querySelector('.item-galery img[data-color="' + color + '"]').click();
    });
};


$('.item-galery img').click(function(e){
    $('img').removeClass('activ-slide-color');
    $('img[data-color='+e.target.getAttribute('data-color-slider')+']').addClass('activ-slide-color');
});
    // catalog image tab

// Попап сертефикат
$('.image-popup-zoom').magnificPopup({
   type: 'image',
   zoom: {
       enabled: true,
     duration: 300 // продолжительность анимации. Не меняйте данный параметр также и в CSS
 }
});



// Попап сертефикат


var owlS = $('.owl-carousel-brend');
owlS.owlCarousel({
    loop:true,
    dots:false,
    margin:0,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:3
        },
        1000:{
            items:3
        }
    }
});
$('.next-serteficate').click(function(){
    owlS.trigger('next.owl.carousel');
})

$('.prev-serteficate').click(function(){
    owlS.trigger('prev.owl.carousel');
});

var owl = $('.owl-carousel-galery')
owl.owlCarousel({
    loop:false,
    dots:false,
    autoplay:true,
    margin:20,
    nav:false,
    responsive:{
        0:{
            items:4
        },
        768:{
            items:4
        },
        1000:{
            items:4
        }
    }
});





if(requestFormBtn) {
    requestFormBtn.click(function() {
        var data = new FormData(requestForm[0]);
        var email = requestForm[0].querySelector('input[name=email]');
        var name = requestForm[0].querySelector('input[name=name]');
        var emailErrors = email.nextElementSibling;
        var nameErrors = name.nextElementSibling;
        emailErrors.innerHTML = '';
        nameErrors.innerHTML = '';

        axios.post('/request/email', data)
        .then(function(r) {
            Swal.fire(r.data.message);
            $("#exampleModal").modal('hide');
        })
        .catch(function(err) {
            var errors = err.response.data.errors;
            emailErrors.innerHTML = errors.email;
            nameErrors.innerHTML = errors.name;
        });
    });
}

if(requestContactBtn) {
    requestContactBtn.click(function() {
        var data = new FormData(requestContactForm[0]);
        var phone = requestContactForm[0].querySelector('input[name=phone]');
        var name = requestContactForm[0].querySelector('input[name=name]');
        var phoneErrors = phone.nextElementSibling;
        var nameErrors = name.nextElementSibling;
        phoneErrors.innerHTML = '';
        nameErrors.innerHTML = '';

        axios.post('/contact-form', data)
        .then(function(r) {
            Swal.fire(r.data.message);
        })
        .catch(function(err) {
            var errors = err.response.data.errors;
            phoneErrors.innerHTML = errors.phone;
            nameErrors.innerHTML = errors.name;
        });
    });
}
if(requestFormBtnSecond) {
    requestFormBtnSecond.click(function() {
        var data = new FormData(requestFormSecond[0]);
        var phone = requestFormSecond[0].querySelector('input[name=phone]');
        var name = requestFormSecond[0].querySelector('input[name=name]');
        var phoneErrors = phone.nextElementSibling;
        var nameErrors = name.nextElementSibling;
        phoneErrors.innerHTML = '';
        nameErrors.innerHTML = '';

        axios.post('/request/phone', data)
            .then(function(r) {
                Swal.fire(r.data.message);
            })
            .catch(function(err) {
                var errors = err.response.data.errors;
                phoneErrors.innerHTML = errors.phone;
                nameErrors.innerHTML = errors.name;
            });
    });
}


if(requestProductFormBtn) {
    requestProductFormBtn.click(function() {
        var data = new FormData(requestProductForm[0]);
        var phone = requestProductForm[0].querySelector('input[name=phone]');
        var name = requestProductForm[0].querySelector('input[name=name]');
        var phoneErrors = phone.nextElementSibling;
        var nameErrors = name.nextElementSibling;
        phoneErrors.innerHTML = '';
        nameErrors.innerHTML = '';

        axios.post('/request/product', data)
        .then(function(r) {
            Swal.fire(r.data.message);
            $("#exampleModal1").modal('hide');
        })
        .catch(function(err) {
            var errors = err.response.data.errors;
            phoneErrors.innerHTML = errors.phone;
            nameErrors.innerHTML = errors.name;
        });
    });
}

if(requestThirdFormBtn) {
    requestThirdFormBtn[0].addEventListener('click', function() {
            var data = new FormData(requestThirdForm[0]);
            var phone = requestThirdForm[0].querySelector('input[name=phone]');
            var name = requestThirdForm[0].querySelector('input[name=name]');
            var phoneErrors = phone.nextElementSibling;
            var nameErrors = name.nextElementSibling;
            phoneErrors.innerHTML = '';
            nameErrors.innerHTML = '';

            axios.post('/request/phone', data)
            .then(function(r) {
                Swal.fire(r.data.message);
                $("#exampleModal1").modal('hide');
            })
            .catch(function(err) {
                var errors = err.response.data.errors;
                phoneErrors.innerHTML = errors.phone;
                nameErrors.innerHTML = errors.name;
            });
    });
}
$( document ).ready(function(){


    // catalog image tab
    $('.item-galery img').click(function(){
        let attr = $(this).attr('src');
        $('.item-galery').removeClass('activ-slide');
        $(this).parent('.item-galery').toggleClass('activ-slide');

        $('.galery-main img').attr('src', attr);
    });

    $('.color img').click(function(){
        $('.color img').removeClass('activ-slide-color');
        $(this).toggleClass('activ-slide-color');

    });


    var owl = $('.owl-carousel-brend');

    owl.owlCarousel({
        loop:true,
        dots:false,
        margin:0,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:2
            },
            1000:{
                items:3
            }
        }
    });
    $('.next').click(function(w) {
        w.preventDefault();
        owl.trigger('next.owl.carousel');
    });

    $('.prev').click(function(w) {
        w.preventDefault();
        owl.trigger('prev.owl.carousel',);
    });

    var owl = $('.owl-carousel-galery')

    owl.owlCarousel({
        loop:false,
        dots:false,
        margin:20,
        nav:false,
        responsive:{
            0:{
                items:4
            },
            768:{
                items:4
            },
            1000:{
                items:4
            }
        }
    });
});
