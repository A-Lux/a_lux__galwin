@extends('layouts.app')
@section('title', env('APP_NAME') . ' - ' . $product->name)
@section('content')
<div class="modal-back-call">
	<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLongTitle">Оставить заявку</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form id="request_product_form" action="{{route('request_product')}}" method="POST">
						<input type="text" name="phone" placeholder="Телефон" class="phone">
						<span class="text-danger"></span>
						<input type="text" name="name" placeholder="Имя">
						<span class="text-danger"></span>
						<input type="hidden" name="product_id" value="{{ $product->id }}">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" id="request_product_form_btn">Отправить</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- PAGES -->
<div class="back-pages">
	<div class="container">
		{{ Breadcrumbs::render('product', $product) }}
	</div>
</div>

<!-- PAGES -->

<!-- CATALOG -->
<div class="catalog">
	<div class="container">
		<h1>Каталог</h1>

		<div class="row">
			<div class="col-xl-6">
				<div class="galery-main">
					@if(isset($images[0]))
					<img src="{{ asset('storage/'.$images[0]->image) }}" id="mainGalery" alt="" >
					@endif
				</div>

				<div class="galery-content">
					<div class="owl-carousel owl-carousel-galery owl-theme">
						@foreach ($images as $image)
							<div class="item-galery"><img src="{{ asset('storage/'.$image->image) }}" alt="" data-color="{{ $image->color_id }}"></div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-xl-6">
				<div class="catalog-main">
					<h3>{{ $product->name }}</h3>
					<div class="catalog-content">
						<table>
							<tbody>
								<tr>
									<td>Количество камер</td>
									<td>{{ $product->number_of_cameras }}</td>
								</tr>
								<tr>
									<td>Толщина лицевых стенок</td>
									<td>{{ $product->wall_thickness }}</td>
								</tr>
								<tr>
									<td>Ширина стеклопакета</td>
									<td>{{ $product->glass_unit_width }}</td>
								</tr>
								<tr>
									<td>Монтажная глубина</td>
									<td>{{ $product->system_depth }}</td>
								</tr>
								<tr>
									<td>Максимальная толщина заполнения</td>
									<td>{{ $product->maximum_filling_thickness }}</td>
								</tr>
								<tr>
									<td>Толщина усилительных вкладышей</td>
									<td>{{ $product->thickness_of_reinforcing_inserts }}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="color-content">
						<p>Доступна в цветах</p>
						<div class="color">
							@foreach ($product->colorAvailable as $color)
							<img src="{{ asset('storage/'.$color->color_image) }}" data-color="{{ $color->id }}" data-toggle="tooltip" data-placement="top" title="{{ $color->name }}">
							@endforeach
						</div>
					</div>
					<div class="download">
						<div class="row">
							<div class="col-xl-6 col-md-6">
								<a href="{{ asset('storage/'.$product->drawing) }}" class="popap-drawing" ><div class="download-content"><img src="{{ asset('images/school-material.png') }}" alt=""><p>Чертежи данного товара</p></div></a>
							</div>
							<div class="col-xl-6 col-md-6">
							</div>
							<div class="col-xl-6 col-md-6 mt-3">
								<button class="btn btn-red" data-toggle="modal" data-target="#exampleModal1">Оставить заявку</button>
							</div>
							<div class="col-xl-6 col-md-6 mt-3">
								@php
									$catalog = json_decode($product->catalog);
									$catalog = count($catalog) > 0 ? $catalog[0] : null;
								@endphp
								@if ($catalog)
								<a href="{{ $catalog ? asset('storage/'.$catalog->download_link) : '#' }}" download="{{ $catalog->original_name }}"><div class="download-content-end"><img src="{{ asset('images/download.png') }}" alt=""><p>Скачать каталог</p></div></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="product-text">
			<h3>Информация о товаре</h3>
			{!! $product->description !!}
		</div>
	</div>
</div>


<!-- CATALOG-END -->

<!-- ADVANTAGES -->
@include('partials.advantages')
<!-- ADVANTAGES-END -->
@endsection
