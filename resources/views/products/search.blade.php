@extends('layouts.app')
@section('title', env('APP_NAME') . ' - Поиск')
@section('content')
<!-- PAGES -->
<div class="back-pages">
	<div class="container">
		<div class="pages-link">
			<a href="#">Главная</a>
			<span><img src="images/pages-arrow.png" alt=""></span>
			<a href="#">Каталог</a>
		</div>
	</div>
</div>

<!-- PAGES -->

<!-- PRODUCT-CARD -->
<div class="product-card">
	<div class="container">
            @if($products->count() > 0)
            <div class="row">
            @foreach ($products as $product)
			<div class="col-xl-6 col-md-6">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-xl-4 col-md-12">
							<img src="{{ asset('storage/'.$product->images) }}" class="card-img" alt="{{ $product->name }}">
						</div>
						<div class="col-xl-8 col-md-12">
							<div class="card-body">
								<h5 class="card-title">{{ $product->name }}</h5>
								<ul>
									<li>Количество камер - {{ $product->number_of_cameras }};</li>
									<li>Толщина стенок - {{ $product->wall_thickness }} мм;</li>
									<li>Ширина стеклопакета - {{ $product->glass_unit_width }} мм;</li>
									<li>Системная глубина - {{ $product->system_depth }} мм;</li>
									<li>{{ $product->contours }};</li>
								</ul>
								<div class="color-content-product">
									<p class="card-text"><small class="font-weight-bold">Доступна в цветах</small></p>
									<div class="color">
										@foreach ($product->colorAvailable as $color)
										<img src="{{ asset('storage/'.$color->color_image) }}" alt="{{ $color->name }}">	
										@endforeach
									</div>
								</div>
								<a href="{{ route('product', $product->id) }}" class="btn btn-red">Перейти к серии</a>
							</div>
						</div>
					</div>
				</div>
            </div>
            @endforeach
            </div>
            <div>
                {{ $products->appends(\Request::all())->links() }}
            </div>
            @else
            <h3 class="px-3">По запросу <span style="color: #e60505">{{ \Request::input('q') }}</span> товаров не найдено</h3>
            @endif
		</div>
	</div>
</div>

<!-- PRODUCT-CARD-END -->

<!-- QUESTION -->
@include('partials.request')

<!-- QUESTION-END -->

<!-- ADVANTAGES -->
@include('partials.advantages')
<!-- ADVANTAGES-END -->
@endsection