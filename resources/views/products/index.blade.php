@extends('layouts.app')
@section('title', env('APP_NAME') . ' - Каталог')
@section('content')
<!-- PAGES -->
<div class="back-pages">
	<div class="container">
		{{ \Breadcrumbs::render('products') }}
	</div>
</div>

<!-- PAGES -->
@if(session('success'))
<div class="container">
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>{{session()->get('success')}}</strong>
    </div>
</div>
@endif
<div class="product-card">
	<div class="container">
		<div class="row about-company">
			<div class="col-xl-3">
				<div class="company-card">
					<img src="{{ asset('images/pdf.png') }}" class="card-img">
					<p class="card-title"><b><a class="text-dark" href="{{ asset('storage/'.$files->file1->download_link) }}">{{ $files->file1->original_name }}</a> <small>@if(\File::exists($files->file1->download_link))({{ number_format(\Storage::disk('public')->size($files->file1->download_link) / 10000, 2) }} мб)@endif</small></b></p>
				</div>
			</div>
			<div class="col-xl-3">
				<div class="company-card">
					<img src="{{ asset('images/pdf.png') }}" class="card-img">
					<p class="card-title"><b><a class="text-dark" href="{{ asset('storage/'.$files->file2->download_link) }}">{{ $files->file2->original_name }}</a> <small>@if(\File::exists($files->file2->download_link))({{ number_format(\Storage::disk('public')->size($files->file2->download_link) / 10000, 2) }} мб)@endif</small></b></p>
				</div>
			</div>
			<div class="col-xl-3">
				<div class="company-card">
					<img src="{{ asset('images/pdf.png') }}" class="card-img">
					<p class="card-title"><b><a class="text-dark" href="{{ asset('storage/'.$files->file3->download_link) }}">{{ $files->file3->original_name }}</a> <small>@if(\File::exists($files->file3->download_link))({{ number_format(\Storage::disk('public')->size($files->file3->download_link) / 10000, 2) }} мб)@endif</small></b></p>
				</div>
			</div>
		</div>
		@if($products->count() > 0)
		<div class="row">
			@foreach ($products as $product)
			<div class="col-xl-6 col-md-6">
				<div class="card mb-3" style="max-width: 540px;">
					<div class="row no-gutters">
						<div class="col-xl-4 col-md-12">
							<img src="{{ asset('storage/'.$product->images) }}" class="card-img" alt="{{ $product->name }}">
						</div>
						<div class="col-xl-8 col-md-12">
							<div class="card-body">
								<h5 class="card-title">{{ $product->name }}</h5>
								<ul>
									<li>Количество камер - {{ $product->number_of_cameras }};</li>
									<li>Толщина лицевых стенок - {{ $product->wall_thickness }};</li>
									<li>Ширина стеклопакета - {{ $product->glass_unit_width }};</li>
									<li>Монтажная глубина - {{ $product->system_depth }};</li>
									<li>Максимальная толщина заполнения - {{ $product->maximum_filling_thickness }};</li>
									<li>Толщина усилительных вкладышей - {{ $product->thickness_of_reinforcing_inserts }};</li>
								</ul>
								<div class="color-content-product">
									<p class="card-text"><small class="font-weight-bold">Доступна в цветах</small></p>
									<div class="color">
										@foreach ($product->colorAvailable as $color)
											<img src="{{ asset('storage/'.$color->color_image) }}" alt="{{ $color->name }}" title="{{ $color->name}}">
										@endforeach
									</div>
								</div>
								<a href="{{ route('product', $product->id) }}" class="btn btn-red">Перейти к серии</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>
		<div>
			{{ $products->links() }}
		</div>
		@endif
	</div>
</div>
<div class="whatsapp">
	<a href="https://api.whatsapp.com/send?phone=+77717533246&text=Здравствуйте!%20Пишу%20с%20города"</a>
	<div class="circlephone" style="transform-origin: center;">
	<div class="circle-fill" style="transform-origin: center;">
</div>
<!-- PRODUCT-CARD-END -->

<!-- QUESTION -->
@include('partials.request')

<!-- QUESTION-END -->

<!-- ADVANTAGES
@include('partials.advantages')-->


<!-- ADVANTAGES-END -->
@endsection
