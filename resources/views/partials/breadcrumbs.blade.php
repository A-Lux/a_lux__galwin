@if (count($breadcrumbs))

    <ul class="breadcrumb_style-custom pages-link" style="display: flex; list-style: none">
        @foreach ($breadcrumbs as $breadcrumb)

            @if ($breadcrumb->url && !$loop->last)
                <li class="px-1">
                    <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                    <span><img src="{{ asset('images/pages-arrow.png') }}"></span>
                </li>
            @else
                <li class="px-1"><a href="javascript:void(0)">{{ $breadcrumb->title }}</a></li>
            @endif

        @endforeach
    </ul>

@endif