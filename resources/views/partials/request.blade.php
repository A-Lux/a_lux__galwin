<!-- QUESTION -->
<div class="question">
    <div class="container">
        <div class="col-xl-5">
            <div class="question-content">
                <h1>Есть вопросы?<span>Оставьте заявку</span></h1>
                <p>Мы ответим на все интуресующие вопросы!</p>

                <form action="{{route('request_contact')}}" method="POST" id="request-contact-form">
                    @csrf
                    <input name="name" type="text" placeholder="Имя" required>
                    <span class="text-danger"></span><br>
                    <input name="phone" type="text" placeholder="Телефон" class="phone" required>
                    <span class="text-danger"></span>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-red" id="request-contact-form-btn">Отправить</button>
                </div>
{{--                <form action="{{route('request_contact')}}" method="POST" id="request-contact-form">--}}
{{--                    @csrf--}}
{{--                    <input name="name" type="text" placeholder="Имя" required>--}}
{{--                    <span class="text-danger"></span><br>--}}
{{--                    <input name="phone" type="text" placeholder="Телефон" class="phone" required>--}}
{{--                    <span class="text-danger"></span>--}}
{{--                    <button type="submit" class="btn btn-red" id="request-contact-form-btn">Оставить заявку</button>--}}
{{--                </form>--}}

            </div>
        </div>
    </div>
</div>

<!-- QUESTION-END -->
