<form class="search_box form-inline my-2 my-lg-0" method="GET" action="{{ route('search') }}">
   <input class=" mr-sm-2" type="text" placeholder="Поиск по сайту" aria-label="Search" name="q" value="{{ \Request::input('q') }}">
   <a class="search_btn" href="#">
       <img src="{{ asset('images/search.png') }}" alt="#" onclick="this.parentNode.parentNode.submit()">
   </a>
</form>