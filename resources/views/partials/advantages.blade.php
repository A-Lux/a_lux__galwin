<div class="advantages">
    <div class="container">
        <h1>Наши преимущества</h1>
        <div class="card-main">
            <div class="card-adv" data-aos="fade-up"
     data-aos-duration="500">
                <img src="/images/adv-1.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Долговечность</h5>
                    <p class="card-text">Долговечность профиля поливинилхлоридного
                        для оконных и дверных блоков ТМ galwin в
                        соответствии с режимом III ГОСТ 30973-2002
                    составляет 60 условных лет эксплуатации.</p>
                </div>
            </div>
            <div class="card-adv" data-aos="fade-up"
     data-aos-duration="800">
                <img src="/images/adv-2.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Безопасность</h5>
                    <p class="card-text">Профиль galwin совместим со всеми видами противовзломной фурнитуры любых европейских
                    брендов.</p>
                </div>
            </div>
            <div class="card-adv" data-aos="fade-up"
     data-aos-duration="1200">
                <img src="/images/adv-3.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Прочность</h5>
                    <p class="card-text">Высокую прочность обеспечивает жёсткость
                        применяемого армирования. Жёсткость армирования в импосте - одно из самых важных
                    элементов окна с точки зрения ветровой нагрузки.</p>
                </div>
            </div>
            <div class="card-adv" data-aos="fade-up"
     data-aos-duration="1500">
                <img src="/images/adv-4.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Качество</h5>
                    <p class="card-text">Оконный профиль galwin не меняет цвет, не желтеет.
                        Продукт классифицируется как «Высококачественный
                    оконный профиль».</p>
                </div>
            </div>
            <div class="card-adv" data-aos="fade-up"
     data-aos-duration="1800">
                <img src="/images/adv-5.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Экологичность</h5>
                    <p class="card-text">Миграция ингредиентов в объекты
                        окружающей среды (воздух, вода и пр.)
                    полностью отсутствует.</p>
                </div>
            </div>
        </div>
    </div>
</div>
    