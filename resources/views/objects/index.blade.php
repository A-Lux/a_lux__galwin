@extends('layouts.app')
@section('title', env('APP_NAME') . ' - Объекты')
@section('content') 
<div class="back-pages">
    <div class="container">
        {{ Breadcrumbs::render('objects') }}
    </div>
</div>
<div class="container">
    <div class="galery">
        <h1>Галерея объектов</h1>
        <div class="row">
            <script type="text/javascript">
                var array = []
            </script>
            @foreach ($objects as $object)
            <div class="col-xl-4 col-md-6">
                <div class="card">
                    <div class="popup-gallery{{ $loop->iteration }}">
                        <a href="{{ asset('storage/'. $object->image) }}" rel="alternate"><img src="{{ asset('storage/'. $object->image) }}" class="galery-img" alt="{{ $object->name }}"></a>
                        @if($object->images)
                        @foreach (json_decode($object->images) as $image)
                            <a href="{{ asset('storage/'.$image) }}" rel="alternate"><img src="{{ asset('storage/'.$image) }}" class="galery-img d-none" alt="{{ $object->name }}" /></a>
                        @endforeach
                        @endif
                    </div>
                    <a href="/" target="_blank" style="text-decoration: none; color: #212529;" onmouseover="this.style.textDecoration='none'">
                        <div class="card-body">
                        <h5 class="card-title">{{ $object->title }}</h5>
                        <p class="card-text">{{ $object->short_description }}</p>
                    </div>
                    </a>
                </div>
                <script type="text/javascript">
                    array.push({{ $loop->iteration }});
                </script>
            </div>
            @endforeach              
        </div>
    </div>
</div>

<!--
@include('partials.advantages')

 ADVANTAGES-END -->
@endsection
@push('scripts')
<script>
    for(var i of array) {
        $('.popup-gallery'+i).magnificPopup({
               delegate: 'a',
               type: 'image',
               tLoading: 'Загрузка изображения #%curr%...',
               gallery: {
                   enabled: true,
                   navigateByImgClick: true,
             preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
         }
        });
    }
</script>
@endpush