<!-- FOOTER -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-12">
                <div class="footer-left-block">
                    <img src="{{ asset('images/footer-logo.png') }}" alt="Logo">
                    <p>Высококачественные окна по доступной цене
                       </p>
                    <p>
                         <br>
                        {{ setting('site.address') }}
                    </p>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="row footer-right">
                    <div class="col-xl-6">
                    <form class="form-inline my-2 my-lg-0" method="GET" action="http://galwin.ibeacon.kz/search">
    <input class="form-control mr-sm-2" type="text" placeholder="Поиск по сайту" aria-label="Search" name="q" value="">
    <img src="http://galwin.ibeacon.kz/images/search.png" alt="" onclick="this.parentNode.submit()">
</form>
                    </div>
                    <div class="col-xl-3 footer-right-1">
                        <h5>{{ setting('site.phone') }} </h5>
                        <p>{{ setting('site.email') }}</p>
                    </div>
                    <div class="col-xl-3">
                        <button class="btn btn-red" data-toggle="modal" data-target="#exampleModal">Оставить заявку</button>
                    </div>
                    @php 
                        $menu = menu('galwin-footer', '_json')->chunk(2);
                    @endphp
                    @foreach ($menu as $menu_item)
                        <div class="col-xl-2 col-6 footer-right-2">
                            @foreach ($menu_item as $item)
                            <a href="{{ url($item->url) }}">{{ $item->title }}</a><br>
                            @endforeach
                        </div>
                    @endforeach
                    <div class="col-xl-3 footer-right-social">
                        <p>Подписывайтесь на нас 
                        в соцсетяx</p>
                    </div>
                    <div class="col-xl-3 footer-right-social">
                        
                        <a href="https://www.instagram.com/siegeria/" target="blank"><img src="{{ asset('images/insta.png') }}" alt="instagram"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="footer-end">
    <div class="container">
        <div class="row">
            <div class="col-xl-6"><p>(с) 2019. — Галвин. Все права защищены</p></div>
            <div class="col-xl-6 text-right">
                <a href="{{ url('products') }}"><p>Каталог</p></a>
                <p>Карта сайта</p>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER-END -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/cascade-slider.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
@stack('scripts')
</body>
</html>