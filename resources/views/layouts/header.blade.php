<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>@yield('title', env('APP_NAME'))</title>

	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('css/cascade-slider.css') }}">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Высококачественный ПВХ профиль для окон">

	<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
</head>

<body>
	<!-- HEADER -->
	<div class="container">
		<div class="burger-menu">
			<a href="#" class="burger-menu-btn">
				<span class="burger-menu-lines"></span>
			</a>
		</div>

		<div class="header">
			<div class="row">
				<div class="col-xl-2 col-md-3">
					<div class="logo">
						<a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}" alt=""></a>
					</div>

				</div>
				<div class="col-xl-3 col-md-2">
					<p>Высококачественный ПВХ профиль для оконных и дверных систем</p>
				</div>
				<div class="col-xl-3 col-md-2">
					<div class="text-right header-contacts">
						<p><img src="{{ asset('images/envelope.png') }}" alt="Пишите нам:">Пишите нам:</p>
						<a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
					</div>
				</div>
				<div class="col-xl-2 col-md-2">
					<div class="text-right header-contacts">
						<p><img src="{{ asset('images/whatsapp.png') }}" alt="Звоните нам:">Звоните нам:</p>
						<a class="font-weight-bold" href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a>
					</div>
				</div>
				<div class="col-xl-2 col-md-3">
					<button class="btn btn-red" data-toggle="modal" data-target="#exampleModal">Оставить заявку</button>
				</div>
			</div>
		</div>
	</div>
	<!---END-HEADER -->

	<!-- NAV-PANEL -->
	<div class="nav-panel">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav mr-auto">

						@foreach (menu('galwin-header', '_json') as $item)
						<li class="nav-item">
							<a class="link custom-hover" href="{{ url($item->url) }}">
								<span>{{ $item->title }}</span>
							</a>
						</li>
						@endforeach
					</ul>
					@include('partials.search-form')
				</div>
			</nav>
		</div>
	</div>


	<div class="nav-panel-mobil">
		<div class="container">
			<nav class="navbar-expand-lg navbar-light">
				<ul class="navbar-nav navbar-mobile">
					@foreach (menu('galwin-header', '_json') as $item)
					<li class="nav-item">
						<a class="nav-link" href="{{ url($item->url) }}">{{ $item->title }}</a>
					</li>
					@endforeach
				</ul>

			</nav>
			<div>
				<form class="form-inline my-2 my-lg-0">
					<input class="form-control mr-sm-2" type="text" placeholder="Поиск по сайту" aria-label="Search">
					<img src="{{ asset('images/search.png') }}" alt="">
				</form>
				<p><i class="fas fa-envelope"></i>Пишите нам:</p>
				<a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>

				<p><img src="{{ asset('images/whatsapp.png') }}" alt="Звоните нам:">Звоните нам:</p>
				<h6 class="font-weight-bold">{{ setting('site.phone') }}</h6>
				<button class="btn btn-red" data-toggle="modal" data-target="#exampleModal">Оставить заявку</button>
			</div>

		</div>
	</div>
	<!-- NAV-PANEL-END -->


	<!-- MODAL -->
	<div class="modal-back-call">
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle">Оставить заявку</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
{{--						<form id="request_form">--}}
{{--							<input type="email" name="email" placeholder="Ваш Email">--}}
{{--							<span class="text-danger"></span>--}}
{{--							<input type="text" name="name" placeholder="Ваше Имя">--}}
{{--							<span class="text-danger"></span>--}}
{{--						</form>--}}
                        <form id="request_form1" method="POST" action="{{route('request_phone')}}">
                            <input name="name" type="text" placeholder="Имя">
                            <span class="text-danger"></span><br>
                            <input name="phone" type="text" placeholder="Телефон" class="phone">
                            <span class="text-danger"></span>
                        </form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-red" id="request_form1_btn">Отправить</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL-END -->
