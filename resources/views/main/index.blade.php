@extends('layouts.app')
@section('title', env('APP_NAME') . ' - Главная')
@section('content')

<!-- CAROUSEL-MAIN -->
<div class="main-carousel">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @foreach ($sliders as $slider)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration-1 }}" class="{{ $loop->first ? 'active' : '' }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @foreach ($sliders as $slider)
            <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                <img src="{{ asset('storage/'.$slider->image) }}" class="d-block w-100" alt="{{ $slider->title }}">
                <div class="carousel-caption text-left carousel-text" data-aos="zoom-in-up">
                    <h1>{{ $slider->header }}</h1>
                    <p>{{ $slider->sub_header }}</p>
                    <a href="{{ url($slider->link) }}" class="nav-link p-0 text-white"><button class="btn btn-red">{{ $slider->button_heading }}</button></a>
                </div>
            </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon prev-main" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon next-main" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div   > 

<!-- CAROUSEL-MAIN-END -->


<!--INFORMATION=BLOCK  -->
<!-- <div class="information-block">
    <div class="container">
        <div class="row">
            <div class="col-xl-4" data-aos="fade-up"
     data-aos-duration="300">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('images/clock.png') }}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-7">
                        <div>
                            <h5 class="card-title font-weight-bold">Долговечность <i class="far fa-circle"></i></h5>
                            <p class="card-text">Долговечность профиля для оконных составляет 60 условных лет эксплуатации</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4" data-aos="fade-up"
     data-aos-duration="500">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('images/security.png') }}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div>
                            <h5 class="card-title font-weight-bold">Безопасность<i class="far fa-circle"></i></h5>
                            <p class="card-text">Профиль galwin совместим со всеми видами противовзломной фурнитуры любых европейских брендов</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4" data-aos="fade-up"
     data-aos-duration="700">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('images/plant.png') }}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div>
                            <h5 class="card-title font-weight-bold">Экологичность <i class="far fa-circle"></i></h5>
                            <p class="card-text">Миграция ингредиентов в объекты окружающей среды (воздух вода и пр) полностью отсутствует</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!--INFORMATION=BLOCK-END  -->

<!-- COMPANY -->
<div class="company">
    <div class="container">
        <div class="company-block col-xl-4 col-12" data-aos="fade-right">
            <img src="{{ asset('images/bg-item.png') }}" alt="" class="bg-item">
            <h1>О компании</h1>
            <p><b>Завод «galwin» был открыт в 2006 году 
                как крупный инвестиционный проект 
                по производству пластиковых профилей 
            для окон. </b>Наша компания представляет
            ПВХ-профили торговых марок «galwin»,
        «ROSSI»и «vitro»</p>
        <a href="{{ route('about') }}"><button class="btn btn-light">Узнать подробнее</button></a>
    </div>
    
</div>
</div>
<!-- COMPANY-END -->
<!-- ADVANTAGES -->
@include('partials.advantages')
<!-- ADVANTAGES-END -->



<!-- MAP-CONTACTS -->
<div class="map-contacts">
    <div class="container">
        <div class="map-content">
            <h1>Контакты</h1>
            <p>050019, г. Алматы, мкр. Атырау, 10</p>
            <h3>{{ setting('site.phone') }}</h3>
            <a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a>
            <button class="btn btn-red" data-toggle="modal" data-target="#exampleModal">Обратный звонок</button>
        </div>
    </div>
    <!-- <div id="map" style="width:100%; height:400px"></div> -->

    <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940000794248/center/76.98148012161256,43.29143544796721/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a><div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940000794248/photos/9429940000794248/center/76.98148012161256,43.29143544796721/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div><div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.984097,43.292547/zoom/16/routeTab/rsType/bus/to/76.984097,43.292547╎Sieger WDF, торгово-производственная компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Sieger WDF, торгово-производственная компания</a></div><script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script><script charset="utf-8">new DGWidgetLoader({"width":"100%","height":600,"borderColor":"#a3a3a3","pos":{"lat":43.29143544796721,"lon":76.98148012161256,"zoom":16},"opt":{"city":"almaty"},"org":[{"id":"9429940000794248"}]});</script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
</div>
<div class="whatsapp">
    <a href="https://api.whatsapp.com/send?phone=+77717533246&text=Здравствуйте!%20Пишу%20с%20города"</a>
    <div class="circlephone" style="transform-origin: center;">
    <div class="circle-fill" style="transform-origin: center;">
</div>

<!-- MAP-CONTACTS-END -->

<!-- COMPANY -->
<div class="company-main">
    <div class="container">
        <h1>О компании</h1>
        <br>
        {!! $about->short_description !!}
    </div>
</div>

<!-- COM[ANY-END -->
@endsection