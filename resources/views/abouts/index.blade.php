@extends('layouts.app')
@section('title', env('APP_NAME') . ' - О Компании')
@section('content')
<!-- PAGES -->
<div class="back-pages">
	<div class="container">
		{{ Breadcrumbs::render('about') }}
	</div>
</div>
<!-- PAGES -->

<!-- About-Company -->
<div class="about-company">
	<div class="container">
	<h1>О компании</h1>
	<br>
	<div class="about-company-text">
		<!-- <h3>Мы работаем для вас</h3> -->
		<div class="company-text">
			{!! $about->heading !!}
		</div>
		
		<div class="row">
			<div class="col-xl-6 col-md-12">
				<div class="company-left-img">
                    <img src="{{ asset('storage/'.$about->image) }}">
				</div>
			</div>
			<div class="col-xl-6 col-md-12">
				<div class="company-right-text">
					{!! $about->right_text !!}
				</div>
			</div>
		</div>
		<!-- <div class="about-list">
			<h3>Мы оказываем услуги:</h3>
			<ul>
				<li>
					<img src="{{ asset('images/tick.png') }}" alt="">
					Продажа, аренда и установка газоанализаторов любых типов
				</li>
				<li>
					<img src="{{ asset('images/tick.png') }}" alt="">
					Государственная поверка вашего оборудования в центре стандартизации и метрологии
				</li>
				<li>
					<img src="{{ asset('images/tick.png') }}" alt="">
					Ремонт любой сложности
				</li>
				<li>
					<img src="{{ asset('images/tick.png') }}" alt="">
					Калибровка поверочной сместью на ваших объектах
				</li>
				<li>
					<img src="{{ asset('images/tick.png') }}" alt="">
					Предоставление подменного оборудования на период ремонта
				</li>
			</ul>
		</div> -->
		<div class="production" >
		<div class="container">
			<h1>Фото с производства</h1>
			<div class="production-img">
				<div class="row">
					@foreach ($productionImages as $production)
					<div class="buttons col-12 col-md-3 p-1" data-aos="fade-up"
					data-aos-duration="500">
						<img width="100%" src="{{ asset('storage/'.$production->image) }}" class="bg-item">
					</div>
					@endforeach
				</div>
			</div>
		</div>
</div>

<!-- PRODUCTION-IMG-END -->
	</div>
</div>
</div>
	<div class="whatsapp">
		<a href="https://api.whatsapp.com/send?phone=+77717533246&text=Здравствуйте!%20Пишу%20с%20города"</a>
		<div class="circlephone" style="transform-origin: center;">
		<div class="circle-fill" style="transform-origin: center;">
	</div>
<!-- ADVANTAGES
@include('partials.advantages')-->

<!-- ADVANTAGES-END -->

<!-- About-Company-END -->
@endsection