@component('mail::message')
# Новая заявка

Поступила новая заявка
@component('mail::button', ['url' => 'https://galwin.co/admin/phone-requests'])
    Просмотр новых заявок
@endcomponent

{{--Thanks,<br>--}}
{{--{{ config('app.name') }}--}}
@endcomponent
