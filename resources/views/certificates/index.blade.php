@extends('layouts.app')
@section('title', env('APP_NAME') . ' - Сертификаты')
@section('content')
<div class="back-pages">
    <div class="container">
        {{ Breadcrumbs::render('certificates') }}
    </div>
</div>
<div class="container">
    <div class="serteficate-page">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="slider_container">
                    <div class="certificate-slider">
                        @foreach ($certificates as $certificate)
                        <div class="item">
                            <div class="img">
                            <a class="image-popup-zoom" href="{{ asset('storage/'.$certificate->image) }}">
                              <img width="100%" src="{{ asset('storage/'.$certificate->image) }}" class="bg-item">
                            </a>
                            </div>
                            <a href="{{ asset('storage/'.$certificate->image) }}" class="downl-button" download>Скачать</a>
                        </div>
                        @endforeach
                    </div> 
                          
                </div>
            </div>
        </div>
    </div>
</div>
<div class="whatsapp">
    <a href="https://api.whatsapp.com/send?phone=+77717533246&text=Здравствуйте!%20Пишу%20с%20города"</a>
    <div class="circlephone" style="transform-origin: center;">
    <div class="circle-fill" style="transform-origin: center;">
</div>
<!-- 
@include('partials.advantages')
-->

<!-- ADVANTAGES-END -->
@endsection