<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class File extends Model
{
    protected $fillable = ['file1', 'file2', 'file3'];    
}
