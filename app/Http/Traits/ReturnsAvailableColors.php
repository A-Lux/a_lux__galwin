<?php 

namespace App\Http\Traits;

use App\Color;

trait ReturnsAvailableColors {
    public function getAvailableColors() {
        $images = $this->media ? collect(json_decode($this->media->images)) : collect([]);
        $this->colorAvailable = $images->count() > 0 ? Color::whereIn('id', $images->pluck('color_id'))->get() : collect();
    }
}