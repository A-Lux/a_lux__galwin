<?php

namespace App\Http\Controllers\Admin;

use App\File;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Events\BreadDataUpdated;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class FileController extends VoyagerBaseController
{
    public function store(Request $request) {
        $file1_name = $request->file('file1')[0]->getClientOriginalName();
        $file2_name = $request->file('file2')[0]->getClientOriginalName();
        $file3_name = $request->file('file3')[0]->getClientOriginalName();
        
        $file1 = json_encode([['download_link' => $request->file('file1')[0]->storeAs('files/'.date('FY'), $file1_name, 'public'), 'original_name' => $file1_name]]);
        $file2 = json_encode([['download_link' => $request->file('file2')[0]->storeAs('files/'.date('FY'), $file2_name, 'public'), 'original_name' => $file2_name]]);
        $file3 = json_encode([['download_link' => $request->file('file3')[0]->storeAs('files/'.date('FY'), $file3_name, 'public'), 'original_name' => $file3_name]]);

        File::create([
            'file1' => $file1,
            'file2' => $file2,
            'file3' => $file3
        ]);
        
        return back();
    }

    public function update(Request $request, $id)
    {
        $file = File::find($id);
        if($request->file1) {
            $file1_name = $request->file('file1')[0]->getClientOriginalName();
            $file1 = json_encode([['download_link' => $request->file('file1')[0]->storeAs('files/'.date('FY'), $file1_name, 'public'), 'original_name' => $file1_name]]);
            $file->file1 = $file1;
        }

        if($request->file2) {
            $file2_name = $request->file('file2')[0]->getClientOriginalName();
            $file2 = json_encode([['download_link' => $request->file('file2')[0]->storeAs('files/'.date('FY'), $file2_name, 'public'), 'original_name' => $file2_name]]);
            $file->file2 = $file2;
        }

        if($request->file3) {
            $file3_name = $request->file('file3')[0]->getClientOriginalName();
            $file3 = json_encode([['download_link' => $request->file('file3')[0]->storeAs('files/'.date('FY'), $file3_name, 'public'), 'original_name' => $file3_name]]);
            $file->file3 = $file3;
        }

        $file->save();

        return back();
    }
}
