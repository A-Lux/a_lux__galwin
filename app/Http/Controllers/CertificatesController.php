<?php

namespace App\Http\Controllers;

use App\Certificate;
use App\ProductionImage;
use Illuminate\Http\Request;

class CertificatesController extends Controller
{
    public function index() {
        $certificates = Certificate::get();
        $productionImages = ProductionImage::limit(8)->latest()->get();
        
        return view('certificates.index', compact('certificates', 'productionImages'));
    }
}