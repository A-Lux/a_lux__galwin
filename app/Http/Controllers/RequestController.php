<?php

namespace App\Http\Controllers;

use App\EmailRequest;
use App\PhoneRequest;
use App\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\RequestMail;

class RequestController extends Controller
{
    public function emailRequest(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'name' => 'required|string'
        ]);

        EmailRequest::create([
            'email' => $request->email,
            'name' => $request->name
        ]);
//        \Illuminate\Support\Facades\Mail::to('170103048@stu.sdu.edu.kz')->send(new \App\Mail\RequestMail());
        Mail::to('marketing@siegeria.com')->send(new RequestMail());



        return response(['message' => 'Ваша заявка принята'], 200);
    }

    public function phoneRequest(Request $request) {
        $this->validate($request, [
            'phone' => 'required',
            'name' => 'required|string'
        ]);

        PhoneRequest::create([
            'phone' => $request->phone,
            'name' => $request->name
        ]);
//        \Illuminate\Support\Facades\Mail::to('170103048@stu.sdu.edu.kz')->send(new \App\Mail\RequestMail());
        Mail::to('marketing@siegeria.com')->send(new RequestMail());
//        Mail::to('tair.uralov@mail.ru')->send(new RequestMail());


        return response(['message' => 'Ваша заявка принята'], 200);
    }

    public function productRequest(Request $request) {
        $this->validate($request, [
            'phone' => 'required',
            'name' => 'required|string',
            'product_id' => 'required'
        ]);

        ProductRequest::create([
            'phone' => $request->phone,
            'name' => $request->name,
            'product_id' => $request->product_id
        ]);
//        \Illuminate\Support\Facades\Mail::to('170103048@stu.sdu.edu.kz')->send(new \App\Mail\RequestMail());
        Mail::to('marketing@siegeria.com')->send(new RequestMail());

        return response(['message' => 'Ваша заявка принята'], 200);
    }
    public function contactForm(Request $request)
    {
        $this->validate($request, [
            'phone' => 'required',
            'name' => 'required|string'
        ]);

        PhoneRequest::create([
            'phone' => $request->phone,
            'name' => $request->name
        ]);
//        return back()->with('success','Ваша заявка принята');
//        \Illuminate\Support\Facades\Mail::to('170103048@stu.sdu.edu.kz')->send(new \App\Mail\RequestMail());
        Mail::to('marketing@siegeria.com')->send(new RequestMail());

        return response(['message' => 'Ваша заявка принята'], 200);

    }
}
