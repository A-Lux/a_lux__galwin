<?php

namespace App\Http\Controllers;

use App\File;
use App\About;
use App\Color;
use App\Product;
use App\ProductionImage;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index() {
        $products = Product::paginate(2);
        
        $files = File::first();
        $files->file1 = json_decode($files->file1)[0];
        $files->file2 = json_decode($files->file2)[0];
        $files->file3 = json_decode($files->file3)[0];
        $productionImages = ProductionImage::limit(8)->latest()->get();
        
        $products->getCollection()->transform(function($product, $key) {
            $product->getAvailableColors();
            return $product;
        });

        return view('products.index', compact('products', 'files', 'productionImages'));
    }

    public function show(Product $product) {
        $product->load('media');
        $images = $product->media ? collect(json_decode($product->media->images)) : collect([]);
        
        $product->getAvailableColors();
        
        return view('products.show', compact('product', 'images'));
    }
}
