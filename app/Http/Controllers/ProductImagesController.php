<?php

namespace App\Http\Controllers;

use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductImagesController extends Controller
{
    public function update(Request $request, ProductImage $productImage) {
        $images = json_decode($productImage->images, true);
        for($i = 0; $i < count($request->color); $i++) {
            if(array_key_exists($i, $request->image)) {
                if(isset($images[$i])) {
                    Storage::disk('public')->delete($images[$i]);
                }
                $images[$i]['image'] = $request->image[$i]->store('images'.$request->product_id,'public');
                $images[$i]['color_id'] = $request->color[$i];
            }else {
    
            }
        }
        $productImage->fill([
            'images' => json_encode($images),
            'product_id' => $request->product_id
        ])->save();
        
        return redirect()->back();
    }

    public function store(Request $request) {
        $images = [];
        for($i = 0; $i < count($request->color); $i++) {
            $images[$i]['image'] = $request->image[$i]->store('images'.$request->product_id,'public');
            $images[$i]['color_id'] = $request->color[$i];
        }
        
        ProductImage::create([
            'images' => json_encode($images),
            'product_id' => $request->product_id
        ])->save();
    
        return redirect()->back();
    }

    public function destroy(ProductImage $productImage) {
        $images = json_decode($productImage->images, true);
        foreach($images as $image) {
            Storage::disk('public')->delete($image['image']);
        }
        $productImage->delete();
    
        return redirect()->back();
    }
}
