<?php

namespace App\Http\Controllers;

use App\About;
use App\Trust;
use App\Slider;
use App\ObjectMade;
use App\Certificate;
use App\ProductionImage;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index() {
        $sliders = Slider::get();
        $trusts = Trust::get();
        $objects = ObjectMade::limit(6)->latest()->get();
        $productionImages = ProductionImage::limit(8)->latest()->get();
        $about = About::select('short_description')->first();
        $certificates = Certificate::get();
        
        return view('main.index', compact('sliders', 'trusts', 'objects', 'productionImages', 'about', 'certificates'));
    }
}
