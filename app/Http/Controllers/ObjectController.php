<?php

namespace App\Http\Controllers;

use App\Product;
use App\ObjectMade;
use App\ProductionImage;
use Illuminate\Http\Request;

class ObjectController extends Controller
{
    public function index() {
        $objects = ObjectMade::with('products')->get();
        $productionImages = ProductionImage::limit(8)->latest()->get();
        return view('objects.index', compact('objects', 'productionImages'));
    }

    public function show(ObjectMade $object) {
        return $object->load('products');
    }

    public function getByProduct(Product $product) {
        $objects = $product->objects;
        $productionImages = ProductionImage::limit(8)->latest()->get();
        return view('objects.index', compact('objects', 'productionImages'));
    }
}
