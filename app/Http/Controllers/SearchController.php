<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request) {
        $products = collect([]);
        if(!empty($request->q)) {
            $products = Product::where('name', 'LIKE', '%'.$request->q.'%')->paginate(2);
            $products->getCollection()->transform(function($product, $key) {
                $product->getAvailableColors();
                return $product;
            });
        }

        return view('products.search', compact('products'));
    }
}
