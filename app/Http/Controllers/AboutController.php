<?php

namespace App\Http\Controllers;

use App\About;
use App\ProductionImage;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index() {
        $about = About::first();
        $productionImages = ProductionImage::limit(8)->latest()->get();
        
        return view('abouts.index', compact('about', 'productionImages'));
    }
}
