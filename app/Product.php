<?php

namespace App;

use App\Color;
use App\ObjectMade;
use App\ProductImage;
use Illuminate\Database\Eloquent\Model;
use App\Http\Traits\ReturnsAvailableColors;


class Product extends Model
{
    use ReturnsAvailableColors;
    
    static private $rel = ['objects'];
    static private $relationsMethods = ['colorsRelation'];

    public function media() {
        return $this->hasOne(ProductImage::class);
    }

    public function objects(){
        return $this->belongsToMany(ObjectMade::class, 'product_object');
    }
}
