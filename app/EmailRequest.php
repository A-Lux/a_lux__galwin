<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailRequest extends Model
{
    protected $fillable = ['name', 'phone'];
}
