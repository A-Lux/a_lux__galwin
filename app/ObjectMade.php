<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;


class ObjectMade extends Model
{
    public function products() {
        return $this->belongsToMany(Product::class, 'product_object');
    }
}
