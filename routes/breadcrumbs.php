<?php

Breadcrumbs::for('main', function ($trail) {
    $trail->push('Главная', route('main'));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('main');
    $trail->push('О Нас', route('about'));
});

Breadcrumbs::for('products', function ($trail) {
    $trail->parent('main');
    $trail->push('Каталог', route('products'));
});

Breadcrumbs::for('product', function ($trail, $product) {
    $trail->parent('products');
    $trail->push($product->name, route('product', $product));
});

Breadcrumbs::for('objects', function ($trail) {
    $trail->parent('main');
    $trail->push('Объекты', route('about'));
});

Breadcrumbs::for('certificates', function ($trail) {
    $trail->parent('main');
    $trail->push('Сертификаты', route('about'));
});