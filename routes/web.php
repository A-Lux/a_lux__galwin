<?php
use App\Color;
use App\Product;
use App\ProductImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

//Route::get('/email', function (){
//
//return new \App\Mail\RequestMail();
//});

Route::get('/', 'MainController@index')->name('main');



Route::resource('products', 'ProductController')
    ->names([
        'index' => 'products',
        'show' => 'product'
    ])
    ->only(['index', 'show']);

Route::get('objects', 'ObjectController@index')
    ->name('objects');

Route::get('objects/{product}', 'ObjectController@getByProduct')->name('objects-by-product');

Route::get('search', 'SearchController@search')
    ->name('search');

Route::get('about', 'AboutController@index')
    ->name('about');

Route::get('get/colors', function() {
    return Color::select('id', 'name as text')->get();
});

Route::get('get/products', function() {
    return Product::select('id', 'name as text')->get();
});

Route::put('product-images/{productImage}', 'ProductImagesController@update')->name('product-images.update');
Route::post('product-images', 'ProductImagesController@store')->name('product-images.store');
Route::delete('product-images/{productImage}', 'ProductImagesController@destroy')->name('product-images.destroy');

Route::post('request/phone', 'RequestController@phoneRequest')->name('request_phone');
Route::post('contact-form', 'RequestController@contactForm')->name('request_contact');
Route::post('request/email', 'RequestController@emailRequest');
Route::post('request/product', 'RequestController@productRequest')->name('request_product');
Route::get('certificates', 'CertificatesController@index');
